// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommandAction.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ActionList.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API UActionList : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY()
	UCommandAction* Action;
	UPROPERTY()
	UActionList* NextAction;
	UPROPERTY()
	UActionList* LastAction;
};
