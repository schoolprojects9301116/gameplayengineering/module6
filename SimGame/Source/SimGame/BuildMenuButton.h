// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/Image.h"
#include "Components/Button.h"
#include "PaperSprite.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BuildMenuButton.generated.h"

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBuildChoiceSelectedSignature, UPaperSprite*, ChosenSprite);

/**
 * 
 */
UCLASS()
class SIMGAME_API UBuildMenuButton : public UUserWidget
{
	GENERATED_BODY()
	
public:

	virtual void NativeConstruct() override;

	UPROPERTY(BlueprintAssignable)
	FBuildChoiceSelectedSignature BuildChoiceSelected;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* Button;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UImage* Image;

	UPROPERTY(EditAnywhere)
	UPaperSprite* Sprite;

protected:
	UFUNCTION()
	void ButtonClicked();

	bool setup = false;
};
