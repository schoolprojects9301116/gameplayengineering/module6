// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildMenuButton.h"

void UBuildMenuButton::NativeConstruct()
{
	if (!setup)
	{
		setup = true;
		Button->OnClicked.AddDynamic(this, &UBuildMenuButton::ButtonClicked);
	}
}

void UBuildMenuButton::ButtonClicked()
{
	BuildChoiceSelected.Broadcast(Sprite);
}