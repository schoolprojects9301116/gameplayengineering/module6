// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimGameGameModeBase.generated.h"

class UCommandActionManager;
class UCommandAction;

/**
 * 
 */
UCLASS()
class SIMGAME_API ASimGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(BlueprintReadWrite)
	UCommandActionManager* ActionManager;

	UFUNCTION(BlueprintCallable)
	void AddAction(UCommandAction* action);

	UFUNCTION(BlueprintCallable)
	void UndoAction();

	UFUNCTION(BlueprintCallable)
	void RedoAction();

	UFUNCTION()
	void SetActiveWidget(UWidgetComponent* widget);

protected:

	UPROPERTY(EditAnywhere)
	USoundBase* UndoSound;

	UPROPERTY(EditAnywhere)
	USoundBase* RedoSound;

	UPROPERTY(EditAnywhere)
	USoundBase* AddSound;

	UWidgetComponent* ActiveWidget;

	virtual void StartPlay() override;

};
