// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HoverComponent.generated.h"

class UPaperSpriteComponent;
class UPaperSprite;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SIMGAME_API UHoverComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHoverComponent();

	UFUNCTION()
	void MouseBeginHover(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void MouseEndHover(UPrimitiveComponent* TouchedComponent);

	UFUNCTION(BlueprintCallable)
	void SetEnableHover(bool shouldHover);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(Transient)
	UPaperSpriteComponent* PaperSprite;

	UPROPERTY(EditAnywhere)
	UPaperSprite* NormalSprite;

	UPROPERTY(EditAnywhere)
	UPaperSprite* HoveredSprite;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
