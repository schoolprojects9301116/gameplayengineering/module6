// Copyright Epic Games, Inc. All Rights Reserved.


#include "SimGameGameModeBase.h"

#include "CommandActionManager.h"
#include "CommandAction.h"
#include "Components/WidgetComponent.h"

#include "Kismet/GameplayStatics.h"

void ASimGameGameModeBase::StartPlay()
{
	Super::StartPlay();

	ActionManager = NewObject<UCommandActionManager>(UCommandActionManager::StaticClass());
}

void ASimGameGameModeBase::AddAction(UCommandAction* action)
{
	ActionManager->AddAction(action);
	if (AddSound != nullptr)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), AddSound);
	}
}

void ASimGameGameModeBase::UndoAction()
{
	ActionManager->UndoAction();
	if (UndoSound != nullptr)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), UndoSound);
	}
}

void ASimGameGameModeBase::RedoAction()
{
	ActionManager->RedoAction();
	if (RedoSound != nullptr)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), RedoSound);
	}
}

void ASimGameGameModeBase::SetActiveWidget(UWidgetComponent* widget)
{
	if (ActiveWidget != nullptr && ActiveWidget != widget)
	{
		ActiveWidget->SetHiddenInGame(true);
	}
	ActiveWidget = widget;
}
