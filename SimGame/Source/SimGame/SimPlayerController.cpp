// Fill out your copyright notice in the Description page of Project Settings.


#include "SimPlayerController.h"

#include "SimGameGameModeBase.h"


ASimPlayerController::ASimPlayerController()
{
	bEnableMouseOverEvents = true;
	bShowMouseCursor = true;
	bEnableClickEvents = true;
}

void ASimPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Undo", IE_Pressed, this, &ASimPlayerController::UndoInput);
	InputComponent->BindAction("Redo", IE_Pressed, this, &ASimPlayerController::RedoInput);
}

void ASimPlayerController::UndoInput()
{
	auto gameMode = Cast<ASimGameGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		gameMode->UndoAction();
	}
}

void ASimPlayerController::RedoInput()
{
	auto gameMode = Cast<ASimGameGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		gameMode->RedoAction();
	}
}

