// Fill out your copyright notice in the Description page of Project Settings.


#include "PlaceSpriteAction.h"
#include "PaperSprite.h"
#include "PaperSpriteComponent.h"
#include "GridPieceActor.h"
#include "Components/WidgetComponent.h"

void UPlaceSpriteAction::DoAction()
{
	if (PaperSpriteComponent != nullptr && Sprite != nullptr && WidgetComponent != nullptr)
	{
		previousSprite = !PaperSpriteComponent->bHiddenInGame;
		if (previousSprite)
		{
			LastSprite = PaperSpriteComponent->GetSprite();
		}
		PaperSpriteComponent->SetSprite(Sprite);
		PaperSpriteComponent->SetHiddenInGame(false);
		WidgetComponent->SetHiddenInGame(true);
	}
}

void UPlaceSpriteAction::UndoAction()
{
	if (previousSprite)
	{
		PaperSpriteComponent->SetSprite(LastSprite);
	}
	else
	{
		PaperSpriteComponent->SetHiddenInGame(true);
		PaperSpriteComponent->SetSprite(nullptr);
	}
}