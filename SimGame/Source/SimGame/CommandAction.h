// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommandAction.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API UCommandAction : public UObject
{
	GENERATED_BODY()
public:
	virtual void DoAction();
	virtual void UndoAction();
};
