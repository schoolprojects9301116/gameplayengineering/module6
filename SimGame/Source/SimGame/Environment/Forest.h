// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "3D/GridActorBase.h"
#include "Forest.generated.h"

class ATree;

USTRUCT()
struct FWoodHarvestStruct
{
	GENERATED_BODY()
	bool bDidObtainWood = false;
	int ammount = 0;
};

/**
 * 
 */
UCLASS()
class SIMGAME_API AForest : public AGridActorBase
{
	GENERATED_BODY()
	
public:
	AForest();

	FWoodHarvestStruct GetWood(int dmg, int ammount);

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ATree> treeType;

	UPROPERTY(EditAnywhere)
	UChildActorComponent * tree;

	UPROPERTY()
	ATree* myTree;
};
