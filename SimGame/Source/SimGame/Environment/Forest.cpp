// Fill out your copyright notice in the Description page of Project Settings.


#include "Environment/Forest.h"
#include "Environment/Tree.h"
#include "3D/GridPieceBase.h"

AForest::AForest()
{
	tree = CreateDefaultSubobject<UChildActorComponent>(TEXT("Tree"));
}

void AForest::BeginPlay()
{
	Super::BeginPlay();
	tree->SetChildActorClass(treeType);
	tree->CreateChildActor();
	myTree = Cast<ATree>(tree->GetChildActor());
}

FWoodHarvestStruct AForest::GetWood(int dmg, int ammount)
{
	FWoodHarvestStruct harvest;
	if (IsValid(myTree) && myTree->CutTree(dmg))
	{
		harvest.bDidObtainWood = true;
		harvest.ammount = myTree->HarvestTree(ammount);
		if (myTree->RemainingValue() == 0)
		{
			myTree->Destroy();
			owningTile->RemoveOccupant();
		}
	}

	return harvest;
}