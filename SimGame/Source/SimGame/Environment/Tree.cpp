// Fill out your copyright notice in the Description page of Project Settings.


#include "Environment/Tree.h"

// Sets default values
ATree::ATree()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATree::BeginPlay()
{
	Super::BeginPlay();
	
	woodValue = maxWoodValue;
}

// Called every frame
void ATree::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ATree::CutTree(int dmg)
{
	hp = FMath::Clamp(hp - dmg, 0, hp);
	if (!bChoppedDown)
	{
		PlayChopAnimation();
		if (hp == 0)
		{
			bChoppedDown = true;
			PlayFallAnimation();
		}
	}
	return hp == 0;
}

int ATree::HarvestTree(int ammount)
{
	int actualAmmount = FMath::Min(woodValue, ammount);
	woodValue -= actualAmmount;
	PlayHarvestSounds();
	return actualAmmount;
}

int ATree::RemainingValue()
{
	return woodValue;
}