// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tree.generated.h"

UCLASS()
class SIMGAME_API ATree : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATree();

	bool CutTree(int dmg);
	int HarvestTree(int amount);
	int RemainingValue();

protected:

	UFUNCTION(BlueprintImplementableEvent)
	void PlayChopAnimation();

	UFUNCTION(BlueprintImplementableEvent)
	void PlayFallAnimation();

	UFUNCTION(BlueprintImplementableEvent)
	void PlayHarvestSounds();

	bool bChoppedDown = false;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	int maxWoodValue = 10;

	UPROPERTY(EditAnywhere)
	int hp = 3;

	int woodValue;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
