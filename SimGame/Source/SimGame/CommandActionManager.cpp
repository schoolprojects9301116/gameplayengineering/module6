// Fill out your copyright notice in the Description page of Project Settings.


#include "CommandActionManager.h"

#include "CommandAction.h"
#include "ActionList.h"

void UCommandActionManager::AddAction(UCommandAction* action)
{
	auto nextAction = NewObject<UActionList>(UActionList::StaticClass());
	nextAction->Action = action;
	if (HeadAction == nullptr || CurrentAction == nullptr)
	{
		HeadAction = nextAction;
	}
	else
	{
		CurrentAction->NextAction = nextAction;
		nextAction->LastAction = CurrentAction;
	}
	CurrentAction = nextAction;
	CurrentAction->Action->DoAction();
}

bool UCommandActionManager::UndoAction()
{
	if (CurrentAction != nullptr)
	{
		CurrentAction->Action->UndoAction();
		CurrentAction = CurrentAction->LastAction;
		return true;
	}
	return false;
}

bool UCommandActionManager::RedoAction()
{
	if (CanRedo())
	{
		if (CurrentAction == nullptr)
		{
			CurrentAction = HeadAction;
		}
		else
		{
			CurrentAction = CurrentAction->NextAction;
		}
		CurrentAction->Action->DoAction();
		return true;
	}
	return false;
}

bool UCommandActionManager::CanRedo()
{
	if (HeadAction == nullptr)
	{
		return false;
	}
	else if (CurrentAction == nullptr)
	{
		return true;
	}
	else if (CurrentAction->NextAction == nullptr)
	{
		return false;
	}
	return true;
}
