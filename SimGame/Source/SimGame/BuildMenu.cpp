// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildMenu.h"
#include "Components/WrapBox.h"

void UBuildMenu::SetupMenu(TArray<FBuildInfo> info)
{
	WrapBox->ClearChildren();
	for (auto& buildInfo : info)
	{
		auto buttonWidget = CreateWidget<UBuildMenuButton>(this, ButtonType);
		buttonWidget->Image->SetBrushFromTexture(buildInfo.Texture);
		buttonWidget->Sprite = buildInfo.SpriteType;
		buttonWidget->BuildChoiceSelected.AddDynamic(this, &UBuildMenu::SelectBuildChoice);
		WrapBox->AddChildToWrapBox(buttonWidget);
	}
}

void UBuildMenu::SelectBuildChoice(UPaperSprite* sprite)
{
	BuildChoiceSelected.Broadcast(sprite);
}