// Fill out your copyright notice in the Description page of Project Settings.


#include "StatsBar.h"

#include "Components/TextBlock.h"

void UStatsBar::SetWealth(int wealth)
{
	if (WealthText)
	{
		WealthText->SetText(FText::FromString(FString::Printf(TEXT("%d"), wealth)));
	}
}