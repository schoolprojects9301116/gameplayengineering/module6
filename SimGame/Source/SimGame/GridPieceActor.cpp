// Fill out your copyright notice in the Description page of Project Settings.


#include "GridPieceActor.h"
#include "BuildMenu.h"

#include "PaperSpriteComponent.h"
#include "HoverComponent.h"
#include "Components/WidgetComponent.h"

#include "SimGameGameModeBase.h"
#include "PlaceSpriteAction.h"

// Sets default values
AGridPieceActor::AGridPieceActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PaperSpriteComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("PaperSprite"));
	SetRootComponent(PaperSpriteComponent);

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
	WidgetComponent->SetHiddenInGame(true);
	WidgetComponent->SetupAttachment(PaperSpriteComponent);

	HoverComponent = CreateDefaultSubobject<UHoverComponent>(TEXT("Hover"));

	BuildSpriteComponent = CreateDefaultSubobject <UPaperSpriteComponent>(TEXT("BuildSpriteComponent"));
	BuildSpriteComponent->SetHiddenInGame(true);

	BuildSpriteComponent->SetupAttachment(PaperSpriteComponent);
}

void AGridPieceActor::OnMouseClicked(UPrimitiveComponent* Target, FKey ButtonPressed)
{
	if (WidgetComponent->bHiddenInGame)
	{
		auto gameMode = Cast<ASimGameGameModeBase>(GetWorld()->GetAuthGameMode());
		if (gameMode)
		{
			gameMode->SetActiveWidget(WidgetComponent);
		}
		WidgetComponent->SetHiddenInGame(false);
	}
	else
	{
		WidgetComponent->SetHiddenInGame(true);
	}
}

void AGridPieceActor::MouseBeginHoverBuilt(UPrimitiveComponent* TouchedComponent)
{
	if (TouchedComponent == BuildSpriteComponent)
	{
		BuildSpriteComponent->SetSpriteColor(FLinearColor(1, 0.5, 0.5));
	}
}

void AGridPieceActor::MouseEndHoverBuilt(UPrimitiveComponent* TouchedComponent)
{
	if (TouchedComponent == BuildSpriteComponent)
	{
		BuildSpriteComponent->SetSpriteColor(FLinearColor(1, 1, 1, 1));
	}
}

// Called when the game starts or when spawned
void AGridPieceActor::BeginPlay()
{
	Super::BeginPlay();
	
	auto Menu = Cast<UBuildMenu>(WidgetComponent->GetWidget());
	if (Menu)
	{
		Menu->BuildChoiceSelected.AddDynamic(this, &AGridPieceActor::BuildSprite);
	}

	PaperSpriteComponent->OnClicked.AddDynamic(this, &AGridPieceActor::OnMouseClicked);
	BuildSpriteComponent->OnClicked.AddDynamic(this, &AGridPieceActor::OnMouseClicked);

	BuildSpriteComponent->OnBeginCursorOver.AddDynamic(this, &AGridPieceActor::MouseBeginHoverBuilt);
	BuildSpriteComponent->OnEndCursorOver.AddDynamic(this, &AGridPieceActor::MouseEndHoverBuilt);
}

// Called every frame
void AGridPieceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGridPieceActor::BuildSprite(UPaperSprite* sprite)
{
	WidgetComponent->SetHiddenInGame(false);
	auto gameMode = Cast<ASimGameGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		UPlaceSpriteAction* action = NewObject<UPlaceSpriteAction>(UPlaceSpriteAction::StaticClass());
		action->Sprite = sprite;
		action->PaperSpriteComponent = BuildSpriteComponent;
		action->WidgetComponent = WidgetComponent;
		gameMode->AddAction(action);
	}
}

