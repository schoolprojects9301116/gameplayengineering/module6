// Fill out your copyright notice in the Description page of Project Settings.


#include "MapSpawner.h"

// Sets default values
AMapSpawner::AMapSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMapSpawner::BeginPlay()
{
	Super::BeginPlay();

	auto world = GetWorld();
	for (int x = 0; x < GridWidth; x++)
	{
		for (int y = 0; y < GridHeight; y++)
		{
				FVector spawnPos = GetActorLocation();
				spawnPos.X += (x - y) * tileWidth;
				spawnPos.Y += (x + y) * tileHeight;
				spawnPos.Z += (x + y) * 10;
				world->SpawnActor<AActor>(SpawnType, spawnPos, GetActorRotation());
		}
	}
	
}

// Called every frame
void AMapSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

