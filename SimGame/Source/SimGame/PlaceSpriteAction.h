// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CommandAction.h"
#include "PlaceSpriteAction.generated.h"

class UPaperSpriteComponent;
class AGridPieceActor;
class UPaperSprite;
class UWidgetComponent;

/**
 * 
 */
UCLASS()
class SIMGAME_API UPlaceSpriteAction : public UCommandAction
{
	GENERATED_BODY()
	
public:
	virtual void DoAction() override;
	virtual void UndoAction() override;

	UPaperSpriteComponent* PaperSpriteComponent;
	UPaperSprite* Sprite;
	UWidgetComponent* WidgetComponent;
protected:
	bool previousSprite;
	UPaperSprite* LastSprite;
};
