// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/BuildableActor.h"

ABuildableActor::ABuildableActor()
{
	buildableMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Buildable Mesh"));
	SetRootComponent(buildableMesh);
}