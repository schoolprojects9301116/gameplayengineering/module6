// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/LandGridPiece.h"
#include "RNG/RngNoise.h"


ELandFertility ALandGridPiece::GetFertility() const
{
	return landFertility;
}

void ALandGridPiece::SetGridPos(int x, int y)
{
	Super::SetGridPos(x, y);

	//float value = FMath::PerlinNoise2D(FVector2D(x*100, y*10));
	float value = URngNoise::GetNoise(x, y);
	ELandFertility fertility;
	if (value <= 0.3)
	{
		fertility = ELandFertility::LF_Barren;
	}
	else if (value <= 0.7)
	{
		fertility = ELandFertility::LF_Fertile;
	}
	else
	{
		fertility = ELandFertility::LF_Abbundant;
	}
	SetFertility(fertility);
}

void ALandGridPiece::SetFertility_Implementation(ELandFertility fertility)
{
	landFertility = fertility;
}
