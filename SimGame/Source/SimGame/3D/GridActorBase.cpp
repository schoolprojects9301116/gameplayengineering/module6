// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/GridActorBase.h"

// Sets default values
AGridActorBase::AGridActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGridActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

AGridPieceBase* AGridActorBase::GetTile()
{
	return owningTile;
}

// Called every frame
void AGridActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGridActorBase::Initialize(AGridPieceBase* tile)
{
	if (!bIsInitialized)
	{
		bIsInitialized = true;
		owningTile = tile;
		SetupGridActor();
	}
}

void AGridActorBase::SetupGridActor()
{}
