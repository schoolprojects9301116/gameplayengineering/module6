// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridActorBase.generated.h"

class AGridPieceBase;

UCLASS()
class SIMGAME_API AGridActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridActorBase();

	void Initialize(AGridPieceBase* tile);

	AGridPieceBase* GetTile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	AGridPieceBase* owningTile = nullptr;

	virtual void SetupGridActor();

private:
	bool bIsInitialized = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
