// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "3D/Systems/ResourceGenerationObserver.h"
#include "WoodResourceObserver.generated.h"

class AForest;

/**
 * 
 */
UCLASS()
class SIMGAME_API UWoodResourceObserver : public UResourceGenerationObserver
{
	GENERATED_BODY()
	
public:
	virtual void OnNotify_Implementation(UObject* notifier, EObserverEvent eventType);
	void SetWoodSource(AForest* woodSource);
	void SetCutDmg(int dmg);
	virtual bool ShouldUpdate() override;

protected:
	UPROPERTY()
	AForest* forest;
	int cutDmg;
};
