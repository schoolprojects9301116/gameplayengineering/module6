// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "3D/Systems/ResourceStructures.h"

#include "CoreMinimal.h"
#include "ObserverPattern/ObserverInterface.h"
#include "ResourceGenerationObserver.generated.h"

class AResourceBuilding;
class ASimGame3DGameModeBase;

/**
 * 
 */
UCLASS()
class SIMGAME_API UResourceGenerationObserver : public UObserverInterface
{
	GENERATED_BODY()

public:
	virtual void OnNotify_Implementation(UObject* notifier, EObserverEvent eventType) override;
	void SetIncome(EResourceType resourceType, int ammount);
	virtual bool ShouldUpdate();

protected:
	void AddResources(ASimGame3DGameModeBase* gameMode);

	TMap<EResourceType, int> income;
};
