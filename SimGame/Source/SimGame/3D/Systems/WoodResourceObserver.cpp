// Fill out your copyright notice in the Description page of Project Settings.

#include "3D/Systems/WoodResourceObserver.h"
#include "3D/SimGame3DGameModeBase.h"
#include "Environment/Forest.h"

void UWoodResourceObserver::OnNotify_Implementation(UObject* notifier, EObserverEvent eventType)
{
	if (eventType == OE_SystemTick)
	{
		if (IsValid(forest))
		{
			auto harvest = forest->GetWood(cutDmg, income[EResourceType::RT_Wood]);
			if(harvest.bDidObtainWood)
			{
				if (harvest.ammount < income[EResourceType::RT_Wood])
				{
					SetIncome(EResourceType::RT_Wood, harvest.ammount);
					forest = nullptr;
				}
				AddResources(Cast<ASimGame3DGameModeBase>(notifier));
			}
		}
	}
}

void UWoodResourceObserver::SetWoodSource(AForest* woodSource)
{
	forest = woodSource;
}

void UWoodResourceObserver::SetCutDmg(int dmg)
{
	cutDmg = dmg;
}

bool UWoodResourceObserver::ShouldUpdate()
{
	return forest == nullptr;
}
