// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "3D/Systems/ResourceStructures.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ResourceManagementSystem.generated.h"


class UTexture2D;

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class SIMGAME_API UResourceManagementSystem : public UObject
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
	void AddResource(EResourceType resourceType, int ammount);

	UFUNCTION()
	void RemoveResource(EResourceType resourceType, int ammount);

	UFUNCTION(BlueprintPure)
	int GetResourceCount(EResourceType resourceType) const;

	UFUNCTION(BlueprintPure)
	UTexture2D* GetResourceImage(EResourceType resourceType) const;


protected:
	UPROPERTY()
	TMap<EResourceType, int> _resourceCount;

	UPROPERTY(EditAnywhere)
	TMap<EResourceType, UTexture2D*> _resourceImages;
};
