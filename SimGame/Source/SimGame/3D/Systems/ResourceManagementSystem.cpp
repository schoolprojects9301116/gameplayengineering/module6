// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/Systems/ResourceManagementSystem.h"

void UResourceManagementSystem::AddResource(EResourceType resourceType, int ammount)
{
	if (!_resourceCount.Contains(resourceType))
	{
		_resourceCount.Add(resourceType, 0);
	}
	_resourceCount[resourceType] += ammount;
}

void UResourceManagementSystem::RemoveResource(EResourceType resourceType, int ammount)
{
	if (_resourceCount.Contains(resourceType))
	{
		int newCount = _resourceCount[resourceType] - ammount;
		_resourceCount[resourceType] = newCount > 0 ? newCount : 0;
	}
}

int UResourceManagementSystem::GetResourceCount(EResourceType resourceType) const
{
	return _resourceCount.Contains(resourceType) ? _resourceCount[resourceType] : 0;
}

UTexture2D* UResourceManagementSystem::GetResourceImage(EResourceType resourceType) const
{
	return _resourceImages.Contains(resourceType) ? _resourceImages[resourceType] : nullptr;
}
