// Fill out your copyright notice in the Description page of Project Settings.

#include "3D/Systems/ResourceGenerationObserver.h"

#include "3D/SimGame3DGameModeBase.h"

void UResourceGenerationObserver::OnNotify_Implementation(UObject* notifier, EObserverEvent eventType)
{
	if (eventType == EObserverEvent::OE_SystemTick)
	{
		AddResources(Cast<ASimGame3DGameModeBase>(notifier));
	}
}

void UResourceGenerationObserver::AddResources(ASimGame3DGameModeBase* gameMode)
{
	if (gameMode)
	{
		for (auto& resource : income)
		{
			gameMode->ResourceManager->AddResource(resource.Key, resource.Value);
		}
	}
}

void UResourceGenerationObserver::SetIncome(EResourceType resourceType, int ammount)
{
	income.Add(resourceType, ammount);
}

bool UResourceGenerationObserver::ShouldUpdate()
{
	return false;
}