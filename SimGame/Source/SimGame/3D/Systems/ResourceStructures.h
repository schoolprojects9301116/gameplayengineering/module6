// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ResourceStructures.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EResourceType : uint8
{
	RT_Food = 0 UMETA(DisplayName = "FOOD"),
	RT_Wood = 1 UMETA(DisplayName = "WOOD")
};

ENUM_RANGE_BY_FIRST_AND_LAST(EResourceType, EResourceType::RT_Food, EResourceType::RT_Wood)