// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "3D/GridPieceBase.h"
#include "LandGridPiece.generated.h"

UENUM(BlueprintType)
enum class ELandFertility : uint8
{
	LF_Barren,
	LF_Fertile,
	LF_Abbundant
};

/**
 * 
 */
UCLASS()
class SIMGAME_API ALandGridPiece : public AGridPieceBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ELandFertility GetFertility() const;

	virtual void SetGridPos(int x, int y) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetFertility(ELandFertility fertility);

protected:
	UPROPERTY(EditAnywhere)
	ELandFertility landFertility;

};
