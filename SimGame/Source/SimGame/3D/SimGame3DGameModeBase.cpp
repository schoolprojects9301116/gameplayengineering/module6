// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/SimGame3DGameModeBase.h"
#include "3D/LandGridPiece.h"
#include "RNG/RngNoise.h"

ASimGame3DGameModeBase::ASimGame3DGameModeBase()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void ASimGame3DGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(systemTickInterval);
	GameSystemsObservableTick = NewObject<UObservableObject>(UObservableObject::StaticClass());

	TileManager = NewObject<UTileManager>(UTileManager::StaticClass(), tileManagerType);
	AGridPieceBase::observableTileObject->ClearObservers();
	AGridPieceBase::observableTileObject->AddObserver(TileManager);

	ResourceManager = NewObject<UResourceManagementSystem>(UResourceManagementSystem::StaticClass(), _resourceManagerType);
	ResourceManager->AddResource(EResourceType::RT_Food, 3);
	ResourceManager->AddResource(EResourceType::RT_Wood, 3);

	auto world = GetWorld();
	if (world == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Red, TEXT("NO WORLD FOUND FOR GAME MODE"));
		return;
	}

	URngNoise::Generate(worldProperties.rngSamples.X, worldProperties.rngSamples.Y
		, worldProperties.rngSampleSize.X, worldProperties.rngSamples.Y);

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	for (int x = 0; x < worldProperties.Width; x++)
	{
		for (int y = 0; y < worldProperties.Length; y++)
		{
			float spawnX = (float)x * worldProperties.GridUnitDistance.X;
			float spawnY = (float)y * worldProperties.GridUnitDistance.Y;
			auto gridPiece = world->SpawnActor<AGridPieceBase>(landType, FVector(spawnX, spawnY, 0), FRotator(), spawnParams);
			if (gridPiece != nullptr)
			{
				gridPiece->SetGridPos(x, y);
			}
			if (URngNoise::GetNoise(x, y) > 0.45 && URngNoise::GetNoise(x, y) < 0.55)
			{
				gridPiece->SetOccupant(forestType);
			}
		}
	}

	resourceBar = CreateWidget<UResourceBarWidget>(GetWorld(), _resourceBarType);
	if (resourceBar != nullptr)
	{
		resourceBar->AddToViewport();
	}
}

void ASimGame3DGameModeBase::Tick(float DeltaTime)
{
	if (GameSystemsObservableTick != nullptr)
	{
		GameSystemsObservableTick->Notify(this, EObserverEvent::OE_SystemTick);
		resourceBar->UpdateResources(ResourceManager);
	}
}

void ASimGame3DGameModeBase::UpdateResourceInfo()
{
	resourceBar->UpdateResources(ResourceManager);
}
