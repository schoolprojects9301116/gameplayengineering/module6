// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/SimPlayerPawn.h"

#include "Components/SphereComponent.h"
#include "GameFramework/FloatingPawnMovement.h"

// Sets default values
ASimPlayerPawn::ASimPlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision"));
	movementComponent = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Floating Pawn Movement"));

	sphere->SetCollisionProfileName("Player");
	sphere->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);

	SetRootComponent(sphere);
}

// Called when the game starts or when spawned
void ASimPlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called to bind functionality to input
void ASimPlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("RotateClockwise", this, &ASimPlayerPawn::RotateView);
	PlayerInputComponent->BindAxis("PanRight", this, &ASimPlayerPawn::PanRight);
	PlayerInputComponent->BindAxis("PanForward", this, &ASimPlayerPawn::PanForward);
}

void ASimPlayerPawn::RotateView(float direction)
{
	AddControllerYawInput(direction);
}

void ASimPlayerPawn::PanRight(float direction)
{
	AddMovementInput(GetActorRightVector(), direction);
}

void ASimPlayerPawn::PanForward(float direction)
{
	AddMovementInput(GetActorForwardVector(), direction);
}

