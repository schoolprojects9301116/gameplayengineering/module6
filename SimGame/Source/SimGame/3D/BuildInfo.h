// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "3D/BuildableActor.h"
#include "3D/Systems/ResourceStructures.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BuildInfo.generated.h"

class ABuildableActor;
class UTexture2D;

/**
 * 
 */
USTRUCT(BlueprintType)
struct SIMGAME_API FBuildInfo3D
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ABuildableActor> BuildingType;

	UPROPERTY(EditAnywhere)
	UTexture2D* BuildMenuImage;

	UPROPERTY(EditAnywhere)
	FString BuildingName;

	UPROPERTY(EditAnywhere)
	FText BuildingDescription;

	UPROPERTY(EditAnywhere)
	TMap<EResourceType, int> BuildCost;
};
