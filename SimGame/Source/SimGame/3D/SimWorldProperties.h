// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SimWorldProperties.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct SIMGAME_API FSimWorldProperties
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	int Width;

	UPROPERTY(EditAnywhere)
	int Length;

	UPROPERTY(EditAnywhere)
	FVector2D rngSamples;

	UPROPERTY(EditAnywhere)
	FVector2D rngSampleSize;

	UPROPERTY(EditAnywhere)
	FVector2D GridUnitDistance;
};
