// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/Sim3DPlayerController.h"

ASim3DPlayerController::ASim3DPlayerController()
{
	bEnableMouseOverEvents = true;
	bShowMouseCursor = true;
	bEnableClickEvents = true;
}