// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Environment/Forest.h"
#include "3D/UI/ResourceBarWidget.h"
#include "3D/SimWorldProperties.h"
#include "ObserverPattern/TileManager.h"
#include "3D/Systems/ResourceManagementSystem.h"
#include "ObserverPattern/ObservableObject.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimGame3DGameModeBase.generated.h"

class ALandGridPiece;

/**
 * 
 */
UCLASS()
class SIMGAME_API ASimGame3DGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASimGame3DGameModeBase();

	UPROPERTY()
	UTileManager * TileManager;

	UPROPERTY()
	UResourceManagementSystem* ResourceManager;

	UPROPERTY()
	UObservableObject * GameSystemsObservableTick;

	virtual void Tick(float DeltaTime) override;

	void UpdateResourceInfo();

protected:

	UPROPERTY(EditAnywhere)
	TSubclassOf<UTileManager> tileManagerType;

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AForest> forestType;

	UPROPERTY(EditAnywhere)
	FSimWorldProperties worldProperties;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ALandGridPiece> landType;

	UPROPERTY(EditAnywhere)
	float systemTickInterval = 5.f;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UResourceBarWidget> _resourceBarType;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UResourceManagementSystem> _resourceManagerType;

	UPROPERTY()
	UResourceBarWidget* resourceBar;
};
