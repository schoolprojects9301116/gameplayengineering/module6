// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/GridPieceBase.h"
#include "3D/SimGame3DGameModeBase.h"
#include "3D/HoverComponent3D.h"
#include "3D/UI/ConstructionMenu.h"
#include "Components/WidgetComponent.h"

UObservableObject* AGridPieceBase::observableTileObject = nullptr;

// Called when the game starts or when spawned
AGridPieceBase::AGridPieceBase()
{
	tileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Tile Mesh"));
	BuildMenuWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Build Menu Widget"));
	BuildMenuWidget->SetWidgetSpace(EWidgetSpace::Screen);
	BuildMenuWidget->SetDrawSize(FVector2D(120, 500));
	BuildMenuWidget->SetPivot(FVector2D(-0.5, 0.25));
	BuildMenuWidget->SetHiddenInGame(true);
	gridPieceSpawnLocation = CreateDefaultSubobject<USceneComponent>(TEXT("Occupant Spawn Location"));

	hoverComponent = CreateDefaultSubobject<UHoverComponent3D>(TEXT("Hover Component 3D"));
	gridPieceOccupant = nullptr;

	SetRootComponent(tileMesh);
	BuildMenuWidget->SetupAttachment(tileMesh);
	gridPieceSpawnLocation->SetupAttachment(tileMesh);
	if (observableTileObject == nullptr)
	{
		observableTileObject = NewObject<UObservableObject>(UObservableObject::StaticClass());
	}
}

void AGridPieceBase::BeginPlay()
{
	Super::BeginPlay();

	tileMesh->OnClicked.AddDynamic(this, &AGridPieceBase::OnMouesClicked);
	constructionMenu = Cast<UConstructionMenu>(BuildMenuWidget->GetWidget());
	if (constructionMenu != nullptr)
	{
		constructionMenu->OnConstructSelected.AddDynamic(this, &AGridPieceBase::ConstructBuilding);
	}
}

void AGridPieceBase::SetHoverComponentActive(bool bActive)
{
	hoverComponent->SetEnableHover(bActive);
	if (bActive)
	{
		hoverComponent->ResetMaterial();
	}
}

void AGridPieceBase::OnMouesClicked(UPrimitiveComponent* target, FKey buttonPressed)
{
	auto tile = BuildMenuWidget->bHiddenInGame ? this : nullptr;
	observableTileObject->Notify(tile, EObserverEvent::OE_TileClicked);
}

void AGridPieceBase::OpenMenu(TArray<FBuildInfo3D> buildInfo)
{
	if (constructionMenu != nullptr)
	{
		constructionMenu->SetupMenu(buildInfo);
		BuildMenuWidget->SetHiddenInGame(false);
	}
}

void AGridPieceBase::CloseMenu()
{
	BuildMenuWidget->SetHiddenInGame(true);
}

void AGridPieceBase::ConstructBuilding(FBuildInfo3D info)
{
	if (!IsValid(gridPieceOccupant))
	{
		gridPieceOccupant = GetWorld()->SpawnActor<AGridActorBase>(
			info.BuildingType, gridPieceSpawnLocation->GetComponentLocation(), FRotator());
		if (gridPieceOccupant != nullptr)
		{
			gridPieceOccupant->Initialize(this);
		}
		observableTileObject->Notify(nullptr, EObserverEvent::OE_TileClicked);
	}
}

void AGridPieceBase::RemoveOccupant()
{
	if (IsValid(gridPieceOccupant))
	{
		gridPieceOccupant->Destroy();
	}
	hoverComponent->SetEnableHover(true);
}

void AGridPieceBase::SetOccupant(TSubclassOf<AGridActorBase> occupantType)
{
	if (!IsValid(gridPieceOccupant))
	{
		gridPieceOccupant = GetWorld()->SpawnActor<AGridActorBase>(
			occupantType, gridPieceSpawnLocation->GetComponentLocation(), FRotator());
		if (gridPieceOccupant != nullptr)
		{
			gridPieceOccupant->Initialize(this);
			hoverComponent->SetEnableHover(false);
		}
	}
}

void AGridPieceBase::SetGridPos(int x, int y)
{
	gridX = x;
	gridY = y;
}

int AGridPieceBase::GetGridDistance(AGridPieceBase& other)
{
	int distance = FMath::Abs(gridX - other.gridX);
	distance += FMath::Abs(gridY - other.gridY);
	return distance;
}
