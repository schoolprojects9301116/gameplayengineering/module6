// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Sim3DPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API ASim3DPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ASim3DPlayerController();
};
