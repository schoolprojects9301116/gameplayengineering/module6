// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HoverComponent3D.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SIMGAME_API UHoverComponent3D : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHoverComponent3D();

	UFUNCTION()
	void MouseBeginHover(UPrimitiveComponent* touchedComponent);

	UFUNCTION()
	void MouseEndHover(UPrimitiveComponent* touchedComponent);

	UFUNCTION(BlueprintCallable)
	void SetEnableHover(bool shouldHover);

	UFUNCTION(BlueprintCallable)
	void ResetMaterial();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* hoverMaterial;

	UPROPERTY()
	UStaticMeshComponent* hoverableMesh;

	UPROPERTY()
	UMaterialInterface* normalMaterial;

	UPROPERTY(EditDefaultsOnly)
	int materialIdx;

	bool bShouldHover;
		
};
