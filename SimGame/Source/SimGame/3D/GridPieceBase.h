// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ObserverPattern/ObservableObject.h"
#include "3D/BuildInfo.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridPieceBase.generated.h"

class UHoverComponent3D;
class AGridActorBase;
class UWidgetComponent;
class UConstructionMenu;

UCLASS()
class SIMGAME_API AGridPieceBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridPieceBase();

	UFUNCTION()
	void SetHoverComponentActive(bool bActive);

	static UObservableObject * observableTileObject;

	UFUNCTION()
	void OpenMenu(TArray<FBuildInfo3D> buildInfo);

	UFUNCTION()
	void CloseMenu();

	UFUNCTION()
	virtual void SetGridPos(int x, int y);

	UFUNCTION()
	void SetOccupant(TSubclassOf<AGridActorBase> occupantType);

	UFUNCTION()
	void RemoveOccupant();

	int GetGridDistance(AGridPieceBase& other);

protected:

	UFUNCTION()
	void ConstructBuilding(FBuildInfo3D buildInfo);

	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	UWidgetComponent* BuildMenuWidget;

	UPROPERTY()
	UConstructionMenu* constructionMenu;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* tileMesh;

	UPROPERTY(EditDefaultsOnly)
	USceneComponent* gridPieceSpawnLocation;

	AGridActorBase* gridPieceOccupant;

	UPROPERTY(EditDefaultsOnly)
	UHoverComponent3D* hoverComponent;

	UFUNCTION()
	void OnMouesClicked(UPrimitiveComponent* target, FKey buttonPressed);

	int gridX = 0;
	int gridY = 0;
};
