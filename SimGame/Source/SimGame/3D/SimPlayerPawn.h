// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SimPlayerPawn.generated.h"

class USphereComponent;
class UFloatingPawnMovement;

UCLASS()
class SIMGAME_API ASimPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASimPlayerPawn();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:

	void BeginPlay() override;

	UFUNCTION()
	void RotateView(float direction);

	UFUNCTION()
	void PanForward(float direction);

	UFUNCTION()
	void PanRight(float direction);

	UPROPERTY(EditDefaultsOnly)
	USphereComponent* sphere;

	UPROPERTY(EditDefaultsOnly)
	UFloatingPawnMovement* movementComponent;
};
