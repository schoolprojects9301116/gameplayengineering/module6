// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "3D/Buildings/ResourceBuilding.h"
#include "FarmBuilding.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API AFarmBuilding : public AResourceBuilding
{
	GENERATED_BODY()
	
protected:
	virtual void SetupResourceGeneration() override;
};
