// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/Buildings/FarmBuilding.h"
#include "3D/LandGridPiece.h"


void AFarmBuilding::SetupResourceGeneration()
{
	if (owningTile != nullptr && resourceTickObserver != nullptr)
	{
		auto landPiece = Cast<ALandGridPiece>(owningTile);
		if (landPiece != nullptr)
		{
			switch (landPiece->GetFertility())
			{
			case ELandFertility::LF_Abbundant:
				resourceTickObserver->SetIncome(EResourceType::RT_Food, 3);
				break;
			case ELandFertility::LF_Fertile:
				resourceTickObserver->SetIncome(EResourceType::RT_Food, 1);
				break;
			case ELandFertility::LF_Barren:
				resourceTickObserver->SetIncome(EResourceType::RT_Food, 0);
				break;
			default:
				break;
			}
		}
	}
}