// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "3D/Systems/ResourceGenerationObserver.h"

#include "CoreMinimal.h"
#include "3D/BuildableActor.h"
#include "ResourceBuilding.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API AResourceBuilding : public ABuildableActor
{
	GENERATED_BODY()

public:

protected:
	virtual void SetupGridActor() override;

	virtual void SetupResourceGeneration();

	UPROPERTY(EditAnywhere)
	TSubclassOf<UResourceGenerationObserver> observerType = UResourceGenerationObserver::StaticClass();

	UPROPERTY()
	UResourceGenerationObserver* resourceTickObserver; 
	
};
