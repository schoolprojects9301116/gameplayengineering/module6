// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/Buildings/ForestryBuilding.h"
#include "3D/Systems/WoodResourceObserver.h"
#include "Environment/Forest.h"
#include "3D/GridPieceBase.h"

#include "Kismet/GameplayStatics.h"

void AForestryBuilding::SetupResourceGeneration()
{
	generator = Cast<UWoodResourceObserver>(resourceTickObserver);
	if (generator != nullptr)
	{
		generator->SetCutDmg(cutDamage);
		TArray<AActor*> foundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AForest::StaticClass(), foundActors);
		AActor* closest = nullptr;
		int closestDistance = 0;
		for (auto& actor : foundActors)
		{
			auto tile = Cast<AGridActorBase>(actor)->GetTile();
			auto distance = owningTile->GetGridDistance(*tile);
			if (closest == nullptr)
			{
				if (actor != lastForest && distance <= maxForestDistance)
				{
					closest = actor;
					closestDistance = distance;
				}
			}
			else if (actor != lastForest && distance < closestDistance)
			{
				closest = actor;
				closestDistance = distance;
			}
		}
		if (closest != nullptr)
		{
			lastForest = closest;
			generator->SetWoodSource(Cast<AForest>(closest));
			int income = FMath::Clamp(maxForestDistance - closestDistance, 1, maxForestDistance);
			generator->SetIncome(EResourceType::RT_Wood, income);
		}
	}
}

void AForestryBuilding::Tick(float DeltaTime)
{
	if (generator && generator->ShouldUpdate())
	{
		SetupResourceGeneration();
	}
}
