// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/Buildings/ResourceBuilding.h"

#include "3D/SimGame3DGameModeBase.h"

void AResourceBuilding::SetupGridActor()
{
	Super::SetupGridActor();

	auto gameMode = Cast<ASimGame3DGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		resourceTickObserver = NewObject<UResourceGenerationObserver>(UResourceGenerationObserver::StaticClass(), observerType);
		gameMode->GameSystemsObservableTick->AddObserver(resourceTickObserver);
		SetupResourceGeneration();
	}
}

void AResourceBuilding::SetupResourceGeneration()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(0, 2.f, FColor::Yellow, TEXT("ResourceBuildingBase::SetupResourceGeneration()"));
	}
}