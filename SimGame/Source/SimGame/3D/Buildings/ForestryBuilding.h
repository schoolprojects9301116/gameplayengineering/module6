// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "3D/Buildings/ResourceBuilding.h"
#include "ForestryBuilding.generated.h"

class UWoodResourceObserver;

/**
 * 
 */
UCLASS()
class SIMGAME_API AForestryBuilding : public AResourceBuilding
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void SetupResourceGeneration() override;

	UPROPERTY(EditAnywhere)
	int cutDamage;

	UPROPERTY(EditAnywhere)
	float maxForestDistance = 2;

	UPROPERTY()
	UWoodResourceObserver* generator;

	UPROPERTY()
	AActor* lastForest = nullptr;
};
