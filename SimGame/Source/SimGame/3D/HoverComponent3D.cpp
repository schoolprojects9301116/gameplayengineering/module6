// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/HoverComponent3D.h"

// Sets default values for this component's properties
UHoverComponent3D::UHoverComponent3D()
{
	hoverMaterial = CreateDefaultSubobject<UMaterialInstance>(TEXT("Hover Material"));
	bShouldHover = true;
}


// Called when the game starts
void UHoverComponent3D::BeginPlay()
{
	Super::BeginPlay();

	hoverableMesh = Cast<UStaticMeshComponent>(GetOwner()->GetComponentByClass(
		UStaticMeshComponent::StaticClass()));

	if (hoverableMesh)
	{
		normalMaterial = hoverableMesh->GetMaterial(materialIdx);
		SetEnableHover(true);
		hoverableMesh->OnBeginCursorOver.AddDynamic(this, &UHoverComponent3D::MouseBeginHover);
		hoverableMesh->OnEndCursorOver.AddDynamic(this, &UHoverComponent3D::MouseEndHover);
		//GetOwner()->OnBeginCursorOver.AddDynamic(this, &UHoverComponent3D::MouseBeginHover);
		//GetOwner()->OnEndCursorOver.AddDynamic(this, &UHoverComponent3D::MouseEndHover);
	}
#ifdef WITH_EDITOR
	else
	{
		GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Red, TEXT("Failed to Find Hoverable Mesh"));
	}
#endif
}

void UHoverComponent3D::MouseBeginHover(UPrimitiveComponent* touchedComponent)
{
	if (bShouldHover && hoverableMesh != nullptr)
	{
		normalMaterial = hoverableMesh->GetMaterial(materialIdx);
		hoverableMesh->SetMaterial(materialIdx, hoverMaterial);
	}
}

void UHoverComponent3D::MouseEndHover(UPrimitiveComponent* touchedComponent)
{
	if (bShouldHover && hoverableMesh != nullptr)
	{
		hoverableMesh->SetMaterial(materialIdx, normalMaterial);
	}
}

void UHoverComponent3D::SetEnableHover(bool shouldHover)
{
	bShouldHover = shouldHover;
}

void UHoverComponent3D::ResetMaterial()
{
	if (hoverableMesh != nullptr)
	{
		hoverableMesh->SetMaterial(materialIdx, normalMaterial);
	}
}
