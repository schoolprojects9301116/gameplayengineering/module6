// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "3D/GridActorBase.h"
#include "BuildableActor.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API ABuildableActor : public AGridActorBase
{
	GENERATED_BODY()
	

public:
	ABuildableActor();

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* buildableMesh;
};
