// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "3D/BuildInfo.h"
#include "3D/UI/ConstructionMenuButton.h"
#include "3D/UI/ResourceDisplayWidget.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ConstructionMenu.generated.h"

class UConstructionMenuButton;
class UWrapBox;
class UTextBlock;
class UVerticalBox;

/**
 * 
 */
UCLASS()
class SIMGAME_API UConstructionMenu : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void SetupMenu(TArray<FBuildInfo3D> buildInfo);

	UPROPERTY()
	FConstructOptionSelectedSignature OnConstructSelected;

	virtual void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;

protected:

	UFUNCTION()
	void DisplayChoice(FBuildInfo3D info);

	void UpdateLayout();

	UFUNCTION()
	void BuildButtonPressed();

	UFUNCTION()
	void CancelButtonPressed();

	UFUNCTION()
	void BroadcastChoice(FBuildInfo3D info);

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* BuildButton;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* CancelButton;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UImage* BuildingImg;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UTextBlock* BuildingName;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UTextBlock* BuildingDescription;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UVerticalBox* costWidgetLayout;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UWrapBox* WrapBox;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UConstructionMenuButton> ButtonType;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UResourceDisplayWidget> displayWidgetType;

	bool bChoiceSelected = false;
	FBuildInfo3D currentChoice;
};
