// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "3D/BuildInfo.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ConstructionMenuButton.generated.h"

class UButton;
class UImage;

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FConstructOptionSelectedSignature, FBuildInfo3D, buildInfo);

/**
 * 
 */
UCLASS()
class SIMGAME_API UConstructionMenuButton : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeConstruct() override;

	void SetupButton(FBuildInfo3D info);

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* Button;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UImage* Image;

	UPROPERTY()
	FConstructOptionSelectedSignature OnConstructBuilding;

	FBuildInfo3D buildInfo;

protected:
	UFUNCTION()
	void ButtonClicked();

	bool setup = false;
};
