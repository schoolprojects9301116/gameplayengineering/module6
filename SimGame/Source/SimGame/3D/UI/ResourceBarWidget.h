// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "3D/Systems/ResourceStructures.h"
#include "3D/UI/ResourceDisplayWidget.h"
#include "Components/VerticalBox.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ResourceBarWidget.generated.h"

class UResourceManagementSystem;

/**
 * 
 */
UCLASS()
class SIMGAME_API UResourceBarWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void UpdateResources(UResourceManagementSystem* resourceManager);

protected:

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UVerticalBox* widgetLayout;

	UPROPERTY()
	TMap<EResourceType, UResourceDisplayWidget*> widgetMap;

	virtual void NativeConstruct() override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UResourceDisplayWidget> _displayWidgetType;
};
