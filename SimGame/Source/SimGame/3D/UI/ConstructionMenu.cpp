// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/UI/ConstructionMenu.h"
#include "3D/UI/ConstructionMenuButton.h"
#include "Components/VerticalBox.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "3D/SimGame3DGameModeBase.h"

#include "Components/WrapBox.h"
#include "Components/Image.h"

void UConstructionMenu::SetupMenu(TArray<FBuildInfo3D> buildInfo)
{
	WrapBox->ClearChildren();
	for (auto& info : buildInfo)
	{
		auto buttonWidget = CreateWidget<UConstructionMenuButton>(this, ButtonType);
		buttonWidget->SetupButton(info);
		buttonWidget->OnConstructBuilding.AddDynamic(this, &UConstructionMenu::DisplayChoice);
		WrapBox->AddChildToWrapBox(buttonWidget);
	}
	BuildButton->OnPressed.AddUniqueDynamic(this, &UConstructionMenu::BuildButtonPressed);
	CancelButton->OnPressed.AddUniqueDynamic(this, &UConstructionMenu::CancelButtonPressed);
	bChoiceSelected = false;
	UpdateLayout();
}

void UConstructionMenu::BroadcastChoice(FBuildInfo3D info)
{
	OnConstructSelected.Broadcast(info);
}

void UConstructionMenu::DisplayChoice(FBuildInfo3D info)
{
	auto gameMode = Cast<ASimGame3DGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		currentChoice = info;
		bChoiceSelected = true;
		UpdateLayout();
	}
}

void UConstructionMenu::UpdateLayout()
{
	auto vis = bChoiceSelected ? ESlateVisibility::Visible : ESlateVisibility::Hidden;

	costWidgetLayout->SetVisibility(vis);
	BuildButton->SetVisibility(vis);
	CancelButton->SetVisibility(vis);
	BuildingImg->SetVisibility(vis);
	BuildingName->SetVisibility(vis);
	BuildingDescription->SetVisibility(vis);

	if (bChoiceSelected)
	{
		costWidgetLayout->ClearChildren();
		BuildingImg->SetBrushFromTexture(currentChoice.BuildMenuImage);
		BuildingName->SetText(FText::FromString(currentChoice.BuildingName));
		BuildingDescription->SetText(currentChoice.BuildingDescription);
		for (auto& cost : currentChoice.BuildCost)
		{
			auto widget = CreateWidget<UResourceDisplayWidget>(this, displayWidgetType);
			if(widget!= nullptr)
			{
				costWidgetLayout->AddChild(widget);
				widget->SetResourceType(cost.Key);
				widget->UpdateResourceCount(cost.Value);
			}
		}
		auto gameMode = Cast<ASimGame3DGameModeBase>(GetWorld()->GetAuthGameMode());
		for (auto& resource : currentChoice.BuildCost)
		{
			if (gameMode->ResourceManager->GetResourceCount(resource.Key) < resource.Value)
			{
				BuildButton->SetVisibility(ESlateVisibility::Hidden);
			}
		}
	}
}

void UConstructionMenu::BuildButtonPressed()
{
	bChoiceSelected = false;
	UpdateLayout();
	auto gameMode = Cast<ASimGame3DGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		for (auto& resource : currentChoice.BuildCost)
		{
			gameMode->ResourceManager->RemoveResource(resource.Key, resource.Value);
		}
		gameMode->UpdateResourceInfo();
	}

	OnConstructSelected.Broadcast(currentChoice);
}

void UConstructionMenu::CancelButtonPressed()
{
	bChoiceSelected = false;
	UpdateLayout();
}

static float updateTime = 3.f;
static float currentTime = 0.f;

void UConstructionMenu::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
	Super::NativeTick(MyGeometry, DeltaTime);
	currentTime += DeltaTime;
	if (currentTime > updateTime)
	{
		if (bChoiceSelected)
		{
			UpdateLayout();
		}
		currentTime = 0.0f;
	}
}
