// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "3D/Systems/ResourceStructures.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ResourceDisplayWidget.generated.h"

class UImage;
class UTextBlock;

/**
 * 
 */
UCLASS()
class SIMGAME_API UResourceDisplayWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetResourceType(EResourceType resourceType);
	void UpdateResourceCount(int newCount);

protected:

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UTextBlock* countText;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage* resourceImg;

	EResourceType _resourceType;
};
