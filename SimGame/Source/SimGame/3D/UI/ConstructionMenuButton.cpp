// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/UI/ConstructionMenuButton.h"
#include "Components/Button.h"
#include "Components/Image.h"

#include "3D/SimGame3DGameModeBase.h"

void UConstructionMenuButton::NativeConstruct()
{
	Super::NativeConstruct();
	Button->OnClicked.AddUniqueDynamic(this, &UConstructionMenuButton::ButtonClicked);
}

void UConstructionMenuButton::SetupButton(FBuildInfo3D info)
{
	buildInfo = info;
	Image->SetBrushFromTexture(info.BuildMenuImage);
}

void UConstructionMenuButton::ButtonClicked()
{
	OnConstructBuilding.Broadcast(buildInfo);
	/*auto world = GetWorld();
	if (world != nullptr)
	{
		auto gameMode = Cast<ASimGame3DGameModeBase>(world->GetAuthGameMode());
		if (gameMode != nullptr)
		{
			bool canBuild = true;
			for (auto& resource : buildInfo.BuildCost)
			{
				if (gameMode->ResourceManager->GetResourceCount(resource.Key) < resource.Value)
				{
					canBuild = false;
				}
			}
			if (canBuild)
			{
			}
			return;
		}
	}*/
}