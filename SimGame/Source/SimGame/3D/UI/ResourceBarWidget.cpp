// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/UI/ResourceBarWidget.h"

#include "3D/SimGame3DGameModeBase.h"

void UResourceBarWidget::NativeConstruct()
{
	Super::NativeConstruct();

	widgetLayout->ClearChildren();

	for (EResourceType resourceType : TEnumRange<EResourceType>())
	{
		auto widget = CreateWidget<UResourceDisplayWidget>(GetWorld(), _displayWidgetType);
		if (widget != nullptr)
		{
			widget->SetResourceType(resourceType);
			widgetLayout->AddChild(widget);
			widgetMap.Add(resourceType, widget);
		}
	}
}

void UResourceBarWidget::UpdateResources(UResourceManagementSystem* resourceManager)
{
	for (auto& widget : widgetMap)
	{
		widget.Value->UpdateResourceCount(resourceManager->GetResourceCount(widget.Key));
	}
}