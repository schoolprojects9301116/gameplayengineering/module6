// Fill out your copyright notice in the Description page of Project Settings.


#include "3D/UI/ResourceDisplayWidget.h"

#include "3D/SimGame3DGameModeBase.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"

void UResourceDisplayWidget::SetResourceType(EResourceType resourceType)
{
	_resourceType = resourceType;

	auto world = GetWorld();
	{
		auto gameMode = Cast<ASimGame3DGameModeBase>(world->GetAuthGameMode());
		if (gameMode != nullptr)
		{
			resourceImg->SetBrushFromTexture(gameMode->ResourceManager->GetResourceImage(resourceType));
			UpdateResourceCount(gameMode->ResourceManager->GetResourceCount(resourceType));
		}
	}
}

void UResourceDisplayWidget::UpdateResourceCount(int newCount)
{
	countText->SetText(FText::FromString(FString::Printf(TEXT("x%d"), newCount)));
}