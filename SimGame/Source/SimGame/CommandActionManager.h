// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CommandActionManager.generated.h"

class UActionList;
class UCommandAction;

/**
 * 
 */
UCLASS()
class SIMGAME_API UCommandActionManager : public UObject
{
	GENERATED_BODY()
public:
	void AddAction(UCommandAction* action);
	bool UndoAction();
	bool RedoAction();
	bool CanRedo();

protected:
	UActionList* HeadAction = nullptr;
	UActionList* CurrentAction = nullptr;
};
