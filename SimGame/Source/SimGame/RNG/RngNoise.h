// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RngNoise.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API URngNoise : public UObject
{
	GENERATED_BODY()

public:

	static void Generate(int numXPoints, int numYPoints, int xSize, int ySize);

	static float GetNoise(float x, float y);

private:
	
	static int width;
	static int height;
	static TArray<float> noiseMap;

};
