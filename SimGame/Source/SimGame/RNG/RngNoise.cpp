// Fill out your copyright notice in the Description page of Project Settings.


#include "RNG/RngNoise.h"

int URngNoise::width = 0;
int URngNoise::height = 0;
TArray<float> URngNoise::noiseMap;


void URngNoise::Generate(int numXPoints, int numYPoints, int xSize, int ySize)
{
	width = numXPoints * xSize + 1;
	height = numYPoints * ySize + 1;
	int numPoints = width * height;
	noiseMap.SetNum(numPoints);

	auto topRight = FMath::RandRange(0.0f, 1.0f);
	float topLeft, botRight, botLeft = 0.0f;
	noiseMap[0] = topRight;
	for (int y = 0; y < height - 1; y++)
	{
		for (int x = 0; x < width - 1; x++)
		{
			int currentIdx = y * width + x;
			float xLerp = ((float)(x % xSize)) / (float(xSize));
			float yLerp = ((float)(y % ySize)) / (float(ySize));

			// CREATE RNG POINTS
			if (x % xSize == 0 && y %ySize == 0)
			{
				if (x == 0)
				{
					botLeft = FMath::RandRange(0.0f, 1.0f);
					noiseMap[currentIdx + width * ySize] = botLeft;
				}
				else
				{
					botLeft = botRight;
				}
				topLeft = topRight;
				topRight = FMath::RandRange(0.0f, 1.0f);
				botRight = FMath::RandRange(0.0f, 1.0f);
				noiseMap[currentIdx + xSize] = topRight;
				noiseMap[currentIdx + width * ySize + xSize] = botRight;
				continue;
			}
			// Only Account for Horizontal Lerp
			else if (y % ySize == 0)
			{
				if (y == 0)
				{
					noiseMap[currentIdx] = FMath::Lerp(topLeft, topRight, xLerp);
				}
				noiseMap[currentIdx + width * ySize] = FMath::Lerp(botLeft, botRight, xLerp);
				continue;
			}
			// Only Account for Vertical Lerp
			else if (x % xSize == 0)
			{
				if (x == 0)
				{
					noiseMap[currentIdx] = FMath::Lerp(topLeft, topRight, yLerp);
				}
				noiseMap[currentIdx + xSize] = FMath::Lerp(topLeft, topRight, yLerp); 
				continue;
			}
			// Blend Lerps
			else
			{
				float leftX = noiseMap[y * width + (x / xSize)];
				float rightX = noiseMap[y * width + (x / xSize) + 1];
				float topY = noiseMap[(y / ySize) * width + x];
				float botY = noiseMap[((y / ySize) + 1) * width + x];
				float value = FMath::Lerp(leftX, rightX, xLerp);
				value += FMath::Lerp(topY, botY, yLerp);
				value /= 2;
				noiseMap[currentIdx] = value;
			}
		}
	}
}

float URngNoise::GetNoise(float x, float y)
{
	return noiseMap[FMath::Clamp(y, 0, height) * height + FMath::Clamp(x, 0, width)];
}

