// Copyright Epic Games, Inc. All Rights Reserved.

#include "SimGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SimGame, "SimGame" );
