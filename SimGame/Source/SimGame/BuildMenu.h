// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperSprite.h"
#include "BuildMenuButton.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BuildMenu.generated.h"

class UWrapBox;


USTRUCT(BlueprintType)
struct FBuildInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperSprite* SpriteType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Texture;
};


/**
 * 
 */
UCLASS()
class SIMGAME_API UBuildMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void SetupMenu(TArray<FBuildInfo> info);
	
	UPROPERTY(BlueprintAssignable)
	FBuildChoiceSelectedSignature BuildChoiceSelected;


protected:
	
	DECLARE_DELEGATE_OneParam(BuildChoiceSelectionDelegate, UPaperSprite*);
	UFUNCTION()
	void SelectBuildChoice(UPaperSprite* sprite);

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UWrapBox* WrapBox;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UBuildMenuButton> ButtonType;
};
