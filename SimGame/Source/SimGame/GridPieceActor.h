// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridPieceActor.generated.h"

class UPaperSpriteComponent;
class UHoverComponent;
class UWidgetComponent;

UCLASS()
class SIMGAME_API AGridPieceActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridPieceActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperSpriteComponent* PaperSpriteComponent;


	UPROPERTY(EditAnywhere)
	UHoverComponent* HoverComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWidgetComponent* WidgetComponent;

	UFUNCTION()
	void OnMouseClicked(UPrimitiveComponent* Target, FKey ButtonPressed);

	UFUNCTION()
	void MouseBeginHoverBuilt(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void MouseEndHoverBuilt(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void BuildSprite(UPaperSprite* sprite);

public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperSpriteComponent* BuildSpriteComponent;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
