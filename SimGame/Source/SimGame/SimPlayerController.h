// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SimPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API ASimPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ASimPlayerController();
	virtual void SetupInputComponent() override;

	UFUNCTION(BlueprintCallable)
	void UndoInput();

	UFUNCTION(BlueprintCallable)
	void RedoInput();

	
};
