// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ObserverPattern/ObserverInterface.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ObservableObject.generated.h"

/**
 * 
 */
UCLASS()
class SIMGAME_API UObservableObject : public UObject
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
	void AddObserver(UObserverInterface* observer);

	UFUNCTION()
	void RemoveObserver(UObserverInterface* observer);

	UFUNCTION()
	void ClearObservers();

	UFUNCTION()
	void Notify(UObject* notifier, EObserverEvent eventType);


private:
	//UPROPERTY()
	UObserverInterface* _headObserver;
};
