// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "3D/BuildInfo.h"

#include "CoreMinimal.h"
#include "ObserverPattern/ObserverInterface.h"
#include "TileManager.generated.h"

class AGridPieceBase;

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class SIMGAME_API UTileManager : public UObserverInterface
{
	GENERATED_BODY()
	
public:
	UTileManager();

	virtual void OnNotify_Implementation(UObject* notifier, EObserverEvent eventType) override;

protected:
	UPROPERTY()
	AGridPieceBase* currentSelection;

	UPROPERTY(EditAnywhere)
	TArray<FBuildInfo3D> AllBuildOptions;
};
