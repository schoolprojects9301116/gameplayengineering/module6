// Fill out your copyright notice in the Description page of Project Settings.


#include "ObserverPattern/ObserverInterface.h"

UObserverInterface::UObserverInterface()
{
	Next = nullptr;
	Last = nullptr;
}

void UObserverInterface::OnNotify_Implementation(UObject* notifier, EObserverEvent eventType)
{}