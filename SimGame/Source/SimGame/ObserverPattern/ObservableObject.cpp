// Fill out your copyright notice in the Description page of Project Settings.


#include "ObserverPattern/ObservableObject.h"

void UObservableObject::AddObserver(UObserverInterface* observer)
{
	if (observer != nullptr)
	{
		observer->Next = _headObserver;
		_headObserver = observer;
	}
}

void UObservableObject::RemoveObserver(UObserverInterface* observer)
{
	if (observer != nullptr)
	{
		if (observer->Last != nullptr)
		{
			observer->Last->Next = observer->Next;
		}
		if (observer->Next != nullptr)
		{
			observer->Next->Last = observer->Last;
		}
		if (observer == _headObserver)
		{
			_headObserver = observer->Next;
		}
	}
}

void UObservableObject::Notify(UObject* notifier, EObserverEvent eventType)
{
	auto currentObserver = _headObserver;
	while (currentObserver != nullptr)
	{
		currentObserver->OnNotify(notifier, eventType);
		currentObserver = currentObserver->Next;
	}
}

void UObservableObject::ClearObservers()
{
	_headObserver = nullptr;
}