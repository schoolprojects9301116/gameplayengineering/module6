// Fill out your copyright notice in the Description page of Project Settings.


#include "ObserverPattern/TileManager.h"

#include "3D/GridPieceBase.h"

UTileManager::UTileManager() : UObserverInterface()
{
	currentSelection = nullptr;
}

void UTileManager::OnNotify_Implementation(UObject* notifier, EObserverEvent eventType)
{
	if (eventType == EObserverEvent::OE_TileClicked)
	{
		auto tile = Cast<AGridPieceBase>(notifier);
		if (currentSelection != nullptr)
		{
			currentSelection->SetHoverComponentActive(true);
			currentSelection->CloseMenu();
		}
		if (tile != nullptr)
		{
			currentSelection = tile;
			currentSelection->SetHoverComponentActive(false);
			currentSelection->OpenMenu(AllBuildOptions);
		}
	}
}