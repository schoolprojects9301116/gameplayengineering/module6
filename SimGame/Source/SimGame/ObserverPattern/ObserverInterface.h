// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ObserverInterface.generated.h"

UENUM(BlueprintType)
enum EObserverEvent
{
	OE_TileClicked UMETA(DisplayName="TileClicked"),
	OE_SystemTick UMETA(DisplayName="System Tick")
};

UCLASS()
class UObserverInterface : public UObject
{
	GENERATED_BODY()

public:
	UObserverInterface();
	
	UFUNCTION(BlueprintNativeEvent)
	void OnNotify(UObject* notifier, EObserverEvent eventType);

	UPROPERTY()
	UObserverInterface* Next;
	UPROPERTY()
	UObserverInterface* Last;
};