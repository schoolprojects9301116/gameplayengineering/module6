// Fill out your copyright notice in the Description page of Project Settings.


#include "HoverComponent.h"
#include "PaperSpriteComponent.h"
#include "PaperSprite.h"

// Sets default values for this component's properties
UHoverComponent::UHoverComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	NormalSprite = CreateDefaultSubobject<UPaperSprite>(TEXT("NormalSprite"));
	HoveredSprite = CreateDefaultSubobject<UPaperSprite>(TEXT("HoveredSprite"));

	// ...
}


// Called when the game starts
void UHoverComponent::BeginPlay()
{
	Super::BeginPlay();

	PaperSprite = Cast<UPaperSpriteComponent>(GetOwner()->GetComponentByClass(UPaperSpriteComponent::StaticClass()));
	if (PaperSprite && NormalSprite)
	{
		PaperSprite->SetSprite(NormalSprite);
		SetEnableHover(true);
	}
	// ...
	
}

void UHoverComponent::MouseBeginHover(UPrimitiveComponent* TouchedComponent)
{
	auto hoveredOn = Cast<UPaperSpriteComponent>(TouchedComponent);
	//if (hoveredOn && hoveredOn == PaperSprite)
	//{
		PaperSprite->SetSprite(HoveredSprite);
	//}
}

void UHoverComponent::MouseEndHover(UPrimitiveComponent* TouchedComponent)
{
	auto hoveredOn = Cast<UPaperSpriteComponent>(TouchedComponent);
	if (hoveredOn && hoveredOn == PaperSprite)
	{
		PaperSprite->SetSprite(NormalSprite);
	}
}


void UHoverComponent::SetEnableHover(bool shouldHover)
{
	if (shouldHover)
	{
		PaperSprite->OnBeginCursorOver.AddDynamic(this, &UHoverComponent::MouseBeginHover);
		PaperSprite->OnEndCursorOver.AddDynamic(this, &UHoverComponent::MouseEndHover);
	}
	else
	{
		PaperSprite->OnBeginCursorOver.RemoveDynamic(this, &UHoverComponent::MouseBeginHover);
		PaperSprite->OnEndCursorOver.RemoveDynamic(this, &UHoverComponent::MouseEndHover);
	}
}

// Called every frame
void UHoverComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

