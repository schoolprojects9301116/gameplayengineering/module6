// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "StatsBar.generated.h"


class UTextBlock;
/**
 * 
 */
UCLASS(Abstract)
class SIMGAME_API UStatsBar : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void SetWealth(int wealth);

protected:

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UTextBlock* WealthText;
};
