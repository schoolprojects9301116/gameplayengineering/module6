// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MapSpawner.generated.h"

UCLASS()
class SIMGAME_API AMapSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> SpawnType;

	UPROPERTY(EditAnywhere)
	float tileWidth;

	UPROPERTY(EditAnywhere)
	float tileHeight;

	UPROPERTY(EditAnywhere)
	int GridWidth;

	UPROPERTY(EditAnywhere)
	int GridHeight;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
