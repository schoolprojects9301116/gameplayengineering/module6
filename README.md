# Kingdom Builder

This repo actually represents two 'projects' in one. Each project was implemented in a single week

#### Project 1 - Castle Builder
The primary goal of the first project was to create a user interface that conformed to the **Usability Pillars** described in Chapter 11 of "The Gamer's Brain" written by Celia Hodent. The design considerations here included making identifiable, and conforming input decisions that naturally portray to the player what is happening and how they can interact with the world.

To properly implement these designs, I had to learn and practice working with the Unreal Engine's UI systems.
Additionally it also served as an excellent time to learn 2d development in Unreal and some of the challenges that acompany it.

#### Project 2 - Kingdom Builder
The second project involved revamping Castle Builder and applying its princples to a 3d game instead. The involved adding additional user commands, adjusting UI to better reflect the game's state based on user feedback from the original game.

I also felt that the UI was hard to truly review without a more involved gameplay experience, so I also created a simple economy system to enhance the experience for better results.

# Project Specifications

The project was created using the Unreal Engine 5.0
